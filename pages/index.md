<!--
.. title: ABOUT
.. slug: index
.. date: 2017-12-24 02:48:11 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->



<img style="float: left;" src="/images/Caitlin-Quigley-240x300.jpg" hspace="20">

Caitlin Quigley does consulting work for social change organizations, cooperatives, and small businesses. She co-founded the Philadelphia Area Cooperative Alliance (PACA), a 501(c)3 cooperative development center and chamber of commerce for cooperatives and credit unions. In 2016, Caitlin was named a winner of the Knight Cities Challenge for her project 20 Book Clubs → 20 Cooperative Businesses, which organized 180 people to study economic cooperation and then form their own cooperative businesses.

Along with two friends, she hosts Solidarity Suppers, a quarterly dinner that raises money for local movements for change. Caitlin serves on the Garden Court Community Association Zoning Committee and the Friends of Lucien E. Blackwell Library. She was recently selected to be a member of the LEADERSHIP Philadelphia Connectors and Keepers Class of 2018. Caitlin occasionally sings in Snarl Honey, Unspun Wool, or backup for Driftwood Soldier. She tweets at @cequigley.
