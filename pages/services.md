<!--
.. title: SERVICES
.. slug: services
.. date: 2017-12-24 02:37:42 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

My services include:

**Communications** *writing, editing, strategy*

**Design** *graphic design for digital and print*

**Social Media** *setup, management, training*

**Research** *interviews, data analysis*


I love working with nonprofits, small businesses, and cooperatives. My clients include:

 * Haverford College
 * Philadelphia Area Cooperative Alliance
 * Bread & Roses Community Fund
 * Philadelphia Reads
 * Cooperative Economics Alliance of New York City
 * Philadelphia Museum of Art
 * Slow Rise Bakery
 * Schumacher Center for a New Economics

Contact me on [LinkedIn](https://www.linkedin.com/in/caitlinquigley/).

